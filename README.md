<div align="center"><strong>Quickly download files from urls in csv files</strong></div>
<div align="center">Quick (and dirty) way to mass download any number of images or other files from cdn for example.</div>

<br />

## Quick start

1.  Make sure that you have Node.js v8.15.1 and npm v5 or above installed.
2.  Clone this repo using `git clone --depth=1 https://gitlab.com/mustikka1/csv-image-downloader <YOUR_PROJECT_NAME>`
3.  Move to the appropriate directory: `cd <YOUR_PROJECT_NAME>`.<br />
4.  Run `npm i` in order to install dependencies.<br />
5.  Create folder called `data` inside project folder and move your .csv file into root of that folder.<br /><br />
Make sure your .csv file contains atleast two colums, one for basename of the downloaded file and one for the url(s) to be downloaded. If there is multiple urls for one "name", you can use `|` as delimiter.<br /><br />
Naming schema follows schema of `${batchPath}/${NAME}_${INDEX ? 'SCE_${index}\' : "MAIN"}.ext` <br /> <br />
    _At this point you can run `npm start` and follow given prompts._ <br /> <br />

Now you're ready to rumble!