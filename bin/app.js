const {parse: parser} = require('csv-parse');
const fs = require('fs')
const prompts = require('prompts');
const axios = require("axios")
const path = require("path")
const glob = require('glob');
const { assert } = require('console');
let file = "data_full.csv"
const randomHash = Math.floor(new Date().getTime() / 1000)

const csvPromise = (filePath) => new Promise((resolve, reject) => {
    fs.readFile(filePath, (err, fileData) => {
      parser(fileData, {}, function(err, rows) {
        resolve(rows) ;
      });
    });
  })

const getCsvPath = async () => {

    const csvFiles = glob.sync('*.csv', {cwd: 'data/'})

    if (!csvFiles) {throw new Error("No csv files found in ./data folder!")}

    file = await prompts({
        type: 'select',
        name: 'value',
        message: 'Which file you want to use?',
        choices: csvFiles,
        initial: 0
      });
    
    return "data/" + csvFiles[file.value]
}


const selectFields = async (data) => {
    const selects = await prompts([
        {
            type: 'select',
            name: 'nameField',
            message: 'Pick Name Field',
            choices: data[0],
            initial: 0
          },
          {
            type: 'select',
            name: 'urlField',
            message: 'Pick Url Field',
            choices: data[0],
            initial: 0
          }
          
        ]);

    return [selects.nameField, selects.urlField]
}

const downloadFile = async (fileUrl, outputLocationPath) => {
    console.log("downloading file:", outputLocationPath)
    return axios({
      method: 'get',
      url: fileUrl,
      responseType: 'stream',
    }).then(function (response) {
        response.data.pipe(fs.createWriteStream(outputLocationPath));
    });
  }

const download = async (data) => {
    const batchPath = `data/${randomHash}`
    await fs.mkdirSync(batchPath);
    for (let i = 0; i < data.length; i++) {
        const line = data[i];

        
        const promises = line.urls.map((url, index) => downloadFile(url, `${batchPath}/${line.name}_${index ? `SCE_${index}` : "MAIN"}${path.extname(url)}`))

        await Promise.all(promises)
    }
}

const main = async () => {

    if (!fs.existsSync("data")) {
        fs.mkdirSync("data");
    }

    const csvPath = await getCsvPath()

    let data = await csvPromise(csvPath)

    data = [data[0], data.slice(1, 10)]

    const [nameField, urlField] = await selectFields(data)

    let toDownload = data[1].map((line)=> {

        if (line[urlField].length < 1) {
            return;
        }

        let urls = line[urlField]

        urls = urls.split("|")

        return {
            name: line[nameField],
            urls 
        }
    }).filter((d) => (d))



    await download(toDownload)
    


    // console.log(data)

    return toDownload
}




main().then((d) => {
    console.log(d)
}).catch((e) => {
    console.error(e)
})